

#include <stdio.h>
#define SPACE "   "
#define STAR " * "

int main()
{
    unsigned    n = 1, 
                i, 
                j;
    
    printf("Input number of rows: ");
    scanf("%u", &n);
    
    for(i = 1; i <= n; i++)
    {
        for(j = 1; j < n + i; j++)
            if(j <= n - i)
                printf(SPACE);
            else 
                printf(STAR);

        printf("\n");
    }
    
    return 0;
}