/*
Написать программу, сортирующую строки (см. задачу 1), но использующую
строки, прочитанные из текстового файла. Результат работы программы также
записывается в файл.
 */

#include <stdio.h>
#include <string.h>

#define N 10
#define M 80

int main()
{
    char str[N][M] = {0};
    
    char* pstr[N];
    char* tmp;
    
    FILE* fpin,
        * fpout;
    
    int i = 0,
        j = 0;
    
    fpin = fopen("in.txt", "rt");
    fpout = fopen("out.txt", "wt");

    if(fpout == NULL || fpin == NULL)
    {
        perror("ERROR");
        return 1;
    }

    for(i = 0; i < N; pstr[i] = str[i], i++)
        ;
    
    i = 0;
    while(i < N && !feof(fpin))
    {
        fgets(str[i], M, fpin);
        i++;
    }
    
    //sort
    for(i = 0; i < N; i++)
        for(j = i; j < N; j++)
            if(strlen(pstr[i]) > strlen(pstr[j]))
            {
                tmp = pstr[i];
                pstr[i] = pstr[j];
                pstr[j] = tmp;
            }

    for(i = 0; i < N; i++)
        fputs(pstr[i], fpout);
    
    printf("DONE!\n");

    fclose(fpin);
    fclose(fpout);
    
    return 0;
}