/*
Написать программу, сортирующую строки (см. задачу 1), но использующую
строки, прочитанные из текстового файла. Результат работы программы также
записывается в файл.
 */

#include <stdio.h>
#include <string.h>

#define N 10
#define M 80

int main()
{
    char str[N][M] = {0};
    
    char* pstr[N];
    char* tmp;
    
    FILE* fpin,
        * fpout;
    
    int i = 0, 
        j = 0, 
        ch;
    
    fpin = fopen("in.txt", "rt");
    if(fpin == NULL)
    {
        perror("ERROR");
        return 1;
    }
    
    fpout = fopen("out.txt", "wt");
    if(fpout == NULL)
    {
        perror("ERROR");
        return 2;
    }

    
    for(i = 0; i < N; pstr[i] = str[i], i++)
        ;
    
    i = 0;
    j = 0;
    
    while(i < N && j < M)
    {
        ch = fgetc(fpin);
        if(ch == '\n')
        {
            str[i][j] = '\n';
            j = 0; i++;
        }
        else if(ch == EOF)
        {
            str[i][j] = '\n';
            break;
        }
        else if(j == M - 2) // [78] = '\n', [79] = '\0'
        {
            str[i][j] = '\n';
            while((ch = fgetc(fpin)) != '\n' && ch != EOF);
            j = 0; i++;
        }
        else 
        {
            str[i][j] = ch;
            j++;
        }
    }
    
    //sort
    for(i = 0; i < N; i++)
        for(j = i; j < N; j++)
            if(strlen(pstr[i]) > strlen(pstr[j]))
            {
                tmp = pstr[i];
                pstr[i] = pstr[j];
                pstr[j] = tmp;
            }

    for(i = 0; i < N; i++)
        fputs(pstr[i], fpout);
    
    printf("DONE!\n");

    fclose(fpin);
    fclose(fpout);
    
    return 0;
}