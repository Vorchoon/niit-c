/*
Написать программу, которая запрашивает строку и определяет, не является
ли строка палиндромом (одинаково читается и слева направо и справа налево)
Замечание:
Цель задачи - применить указатели для быстрого сканирования строки с двух
концов
*/

#include <stdio.h>
#include <string.h>

#define N 256
#define FALSE 0
#define TRUE 1

int ispolindrome(const char* );

int main()
{
    char str[N];
    
    printf("Input your string: ");
    fgets(str, N, stdin);
    
    if(ispolindrome(str))
        printf("It's polindrome!\n");
    else
        printf("It's NOT polindrome!\n");
    return 0;
}

int ispolindrome(const char* str)
{
    char* pstart = str;
    char* pend = &str[strlen(str) - 2];
    
    while(pstart < pend)
        if(*(pstart++) != *(pend--))
            return FALSE;
    return TRUE;
}