/*
1. Написать программу, которая принимает от пользователя строку и
выводит ее на экран, перемешав слова в случайном порядке.
Замечание:
Программа должна состоять минимум из трех функций:
a)
printWord
- выводит слово из строки (до конца строки или пробела)
b)
getWords
- заполняет массив указателей адресами первых букв слов
c)
main
- основная функция
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#define N 256

void printRndWordsInStr(char* );

int main()
{
    char  str[N] = {0};
    
    char* pwords[N] = {NULL};
    
    int nwords = 0,
        i = 0,
        j = 0;
    srand(time(NULL));    
    printf("Input your string: ");
    fgets(str, N, stdin);
    printRndWordsInStr(str);
    putchar('\n');

    return 0;
}