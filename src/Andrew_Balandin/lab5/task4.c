/*
Написать программу, которая читает построчно текстовый файл и
переставляет случайно слова в каждой строке
Замечание:
Программа открывает существующий тектстовый файл и читает его построч-
но. Для каждой строки вызывается функция, разработанная в рамках задачи 1
*/

#include <stdio.h>
#include <string.h>

#define NCHARS 256

void printRndWordsInStr(char*);

int main()
{
    char buf[NCHARS] = {0};
    char* pwords[NCHARS] = {NULL};
    int i, nwords;
    FILE* fpin;
    
    srand(time(NULL));
    fpin = fopen("in.txt", "rt");
    if(fpin == NULL)
    {
        perror("ERROR");
        return 1;
    }
    
    while(!feof(fpin))
    {
        buf[0] = '\0';
        fgets(buf, NCHARS, fpin);
        printRndWordsInStr(buf);
    }
    printf("\n");
    fclose(fpin);

    return 0;
}
