//functions for tasks 1,3,5
#include <string.h>

#define TRUE 1
#define FALSE 0
#define N 256

void printWord(char* pword)
{
    int i = 0;
    while(pword[i] != '\0' && !isspace(pword[i]))
        putchar(pword[i++]);
    /*while(pword[i] != '\0' && pword[i] != '\n' && isspace(pword[i]))
        putchar(pword[i++]);*/
    
}

int getWords(const char* str, const char* pwords[])//OR char** pwords?
{
    int i = 0,
        j = 0,
        inword = FALSE;
    
    for(i = 0; str[i] != '\0'; i++)
    {
        if(!inword && !isspace(str[i]))
        {
            pwords[j++] = &str[i];
            inword = TRUE;
        }
        if(inword && isspace(str[i]))
            inword = FALSE;
    }
    return j;
}

int lenWord(const char* pword)
{
    int len = 0;
    while(*(pword + len) != '\0' && !isspace(*(pword + len)))
        len++;
    return len;
}

void rndCharsInWord(char* pwords)
{
    int i,
        j,
        rnd;
    
    char tmp;
    
    int nchars = lenWord(pwords);
    for(j = 1; j < nchars - 1; j++)
    {
        rnd = rand() % (nchars - 2) + 1;
        tmp = *(pwords + j);
        *(pwords + j) = *(pwords + rnd);
        *(pwords + rnd) = tmp;
    }
}

void rndCharsInStr(char* pwords[], int nwords)
{
    int i;
    for(i = 0; i < nwords; i++)
        rndCharsInWord(pwords[i]);
}


void printRndWordsInStr(char* str)
{
    int i = 0, j = 0;
    char* pwords[N] = {NULL};
    int nwords = getWords(str, pwords);

    while(i < nwords)
    {
        j = rand() % nwords;
        if(pwords[j] != NULL)
        {
            printWord(pwords[j]);
            putchar(' ');
            pwords[j] = NULL;
            i++;
        }
    }
}