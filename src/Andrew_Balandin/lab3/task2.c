/*
Написать программу, которая для введённой строки определяет ко-
личество слов и выводит каждое слово на отдельной строке и его
длину
Замечание:
Слова разделяются любым количеством пробелов.
*/

#include <stdio.h>
#include <ctype.h>

#define N 256
#define TRUE 1
#define FALSE 0

int main()
{
    char str[N] = {0};
    int inword = FALSE,
        i = 0,
        n_w = 0,
        n_ch = 0;
    
    printf("Input text and then put EOF on new line:\n\n\n");
    for(i = 0; (str[i] = getchar()) != EOF && i < N; i++)
    {
        if(!isspace(str[i]) && !inword)
        {
            inword = TRUE;
            n_w++;
        }
        if(isspace(str[i]) && inword)
        {
            inword = FALSE;
            printf(" - contain %d symbols\n", n_ch);
            n_ch = 0;
        }
        if(!isspace(str[i]) && inword)
        {
            putchar(str[i]);
            n_ch++;
        }
    }

    printf("\n\n\nIn your text %d words!\n", n_w);

    return 0;
}
