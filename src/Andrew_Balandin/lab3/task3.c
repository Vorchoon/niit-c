/*
Написать программу, которая для введённой строки выводит самое длинное
слово его длину
*/

#include <stdio.h>

#define N 256
#define TRUE 1
#define FALSE 0

void copyWord(char from[], char to[])
{
    int i;
    for(i = 0; (to[i] = from[i]) != '\0'; i++);
}

int main()
{
    char    str[N] = {0},
            word[N] = {0},
            maxword[N] = {0};
    
    int     i = 0,
            j = 0,
            lenword = 0,
            maxlenword = 0,
            inword = FALSE;
    
    printf("Input your text:\n");
    
    for(i = 0; (str[i] = getchar()) != EOF && i < N; i++)
    {
            if(isspace(str[i]) && inword) //exit from word
            {
                inword = FALSE;
                if(lenword > maxlenword)
                {
                    maxlenword = lenword;
                    copyWord(word, maxword);
                }
                j = 0;
                lenword = 0;
            }
            if(!isspace(str[i]) && !inword) //enter in word
                inword = TRUE;
            if(!isspace(str[i]) && inword) // we are in word
            {
                word[j++] = str[i];
                lenword++;
            }
    }
    
    printf("\nIn your text longest word is \"%s\" contain %d symbols!\n", maxword, maxlenword);
    return 0;
}