/*
Написать программу, которая формирует целочисленный массив размера N,
а затем находит сумму элементов между минимальным и максимальным эле-
ментами.
*/


#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define N 15

int main()
{
    int i = 0,
        nmin = 0,
        min = 0,
        nmax = 0,
        max = 0,
        sum = 0;
    int arr[N] = {0};
    
    srand(time(NULL));
    
    for (i = 0; i < N; i++)
        arr[i] = rand() % 10 + 1;
    
    for (i = 0; i < N; i++)
        printf("%d ", arr[i]);
    printf("\n");
    
    max = min = arr[0];
    for(i = 0; i < N; i++)
    {
        if(arr[i] < min)
        {
            min = arr[i];
            nmin = i;
        }
        if(arr[i] > max)
        {
            max = arr[i];
            nmax = i;
        }
    }
    if(nmin > nmax)
    {
        nmin = nmin + nmax;
        nmax = nmin - nmax;
        nmin = nmin - nmax;
    }
    
    for(i = nmin; i <= nmax; i++)
        sum += arr[i];
    
    printf("Sum = %d\n", sum);
    
    return 0;
}