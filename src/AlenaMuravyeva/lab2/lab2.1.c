/*
 ============================================================================
 Name        : lab2.1.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Program prints "�����"
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
  float startHeight=0.0f;
  const float g=9.81f;
  int time=0;
  float passedDistance=0.0f;

  setlinebuf(stdout);

  printf("Enter height value \n");
  scanf("%f",&startHeight);

  float curHeight=startHeight;

  while (curHeight>0)
  {
    passedDistance=(g*(time*time))/2;
    curHeight=startHeight - passedDistance;
    printf(" t=%02d c",time);

    if(curHeight>0)
    {
      printf("  h = %.1f m\n",curHeight);
    }

    if(curHeight<=0)
    {
      printf("  �����\n");
    }
    time++;
  }
    return 0;
  }
