/*
 ============================================================================
 Name        : lab4.5.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program sorts the lines from a text file
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LETTERS 80
#define MAX_STRINGS 10

int main()
{
   char userString [MAX_STRINGS][MAX_LETTERS];
   FILE *fp;
   char *p[MAX_STRINGS];
   int stringNum=0;
   int stringLen[MAX_STRINGS];
   int hold=0;
   char *save;

   fp=fopen("text1.txt","rt");
   if(fp==NULL)
   {
      perror("File:");
      return 1;
   }

   setlinebuf(stdout);

   while(stringNum<MAX_STRINGS && fgets(userString[stringNum],MAX_LETTERS,fp))
   {
      p[stringNum]=userString[stringNum];
      stringNum++;
   }

   for(int i=0;i<stringNum;i++)
   {
      stringLen[i]=strlen(p[i])-1;
   }

   for(int i=1;i<stringNum;i++)
   {
      for(int j=0; j<stringNum-i;j++)
      {
         if(stringLen[j+1]<stringLen[j])
         {
            hold=stringLen[j];
            save=p[j];
            stringLen[j]=stringLen[j+1];
            p[j]=p[j+1];
            stringLen[j+1]=hold;
            p[j+1]=save;
         }
      }
   }

   fclose(fp);

   fp=fopen("text_out.txt","wt");
   if(fp==NULL)
   {
      perror("File:");
      return 1;
   }

   for (int i=0;i<stringNum;i++)
   {
      fputs(p[i],fp);
   }

   fclose(fp);

   return 0;
}
