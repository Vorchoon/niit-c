/*
 ============================================================================
 Name        :lab4.6.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program defines the very young and the very old relative
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_LETTERS 40
#define MAX_STRINGS 10

int main()
{
   char names[MAX_STRINGS][MAX_LETTERS];
   int relatives=0;
   int currentAge=0;
   char *pYoung = NULL;
   char *pOld = NULL;
   int maxAge=0;
   int minAge = 100;
   int num=0;

   setlinebuf(stdout);

   puts("Enter the number of relatives in the family");
   scanf("%d",&relatives);

   while(num<relatives)
   {
      puts("Enter the name of the relative: ");
      scanf("%s",names[num]);
      puts("Enter the age: ");
      scanf("%d",&currentAge);

      if(currentAge>maxAge)
      {
         maxAge = currentAge;
         pOld = names[num];
      }
      if(currentAge<minAge)
      {
         minAge = currentAge;
         pYoung=names[num];
      }
      num++;
   }

   printf("\nOld relative: %s, age = %d",pOld,maxAge);
   printf("\nYoung relative: %s, age = %d",pYoung,minAge);

   return 0;
}

