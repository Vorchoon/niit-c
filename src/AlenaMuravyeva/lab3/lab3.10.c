/*
 ============================================================================
 Name        : lab3.10.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program deletes the word on the account which the user entered
 ============================================================================
 */

#include <stdio.h>
#include <string.h>

#define TRUE 1
#define FALSE 0
#define ARR_SIZE 256
#define MAX_WORDS 100

int main()
{
   char string [ARR_SIZE];
   int firstLeterIndex=0;
   int secondLeterIndex=0;
   int currentLetter=0;
   int firstFound = TRUE;
   int secondFound = FALSE;
   int numberUser=0;
   int counterWords=0;

   setlinebuf(stdout);

   printf("enter string\n");
   fgets(string,ARR_SIZE,stdin);

   printf("enter number\n");
   scanf("%d",&numberUser);

   if(numberUser<=0 || numberUser>MAX_WORDS)
   {
      printf("Enter correct value\n");
      return 0;
   }

   int len=strlen(string);
   //remove \r and \n
   string[len-2] = '\0';
   len=len-2;
   for(int i=0;i<len;i++)
   {
      if(i==0 && string[i]!=' ')
      {
         firstFound = TRUE;
         firstLeterIndex=i;
      }
      else if(string[i+1]!=' ' && string[i]==' ')
      {
         firstFound = TRUE;
         firstLeterIndex=i+1;
      }
      else if ((string[i]!=' ' && string[i+1]==' ')|| (string[i]!=' ' && string[i+1]=='\0'))
      {
         secondFound = TRUE;
         secondLeterIndex=i;
      }
      if (firstFound == TRUE && secondFound == TRUE)
      {
         counterWords++;
         if(counterWords==numberUser)
         {
            for(int j=firstLeterIndex; j<=secondLeterIndex; j++)
            {
               currentLetter++;
            }
               for(int j=firstLeterIndex; j<=len; j++)
            {
               string[j]=string[j+currentLetter];
            }
         }
         firstFound=FALSE;
         secondFound=FALSE;
         firstLeterIndex=0;
         secondLeterIndex=0;
      }
   }
   string[len-currentLetter+1] = '\0';

   if(numberUser>counterWords)
   {
      printf("Enter correct value\n");
   }
   printf ("%s\n",string);

   return 0;
}
