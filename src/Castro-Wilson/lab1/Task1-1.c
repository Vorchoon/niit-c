/*Программа для расчета для проверка веса человека.
* Для расчетов исползованы формулы Хаммонда, 2000г:
* для мучины 48кг + 1.1кг на 1см роста свыше 150см,
* для женщины 45кг + 0.9кг на 1см роста свыше 150см.
 
 * By Wilson Castro. 2016.
  */
#include <stdio.h>
char sex;
float stature = 0.0f;
float weight = 0.0f;
char get_choice(void);
char get_first(void);
void calculatorM(void);
void calculatorF(void);

int main(void) {
    int choice;
    while ((choice = get_choice()) != 'q') {
        switch (choice) {
            case 'm': printf("Пожалуйста, вводите вашие параметры:\n");

                calculatorM();
                break;
            case 'f': printf("Пожалуйста, вводите вашие параметры:\n");

                calculatorF();
                break;
            default: printf("Ошибка! \n");
                break;
        }
    }
    printf("Программа совершена.\n");
    return 0;
}

char get_first(void) { // функция для чтения ввода 
    int ch;
    ch = getchar();
    while (getchar() != '\n')
        continue;
    return ch;
}

char get_choice(void) { //
    int ch;
    printf("Пожалуйста, введите m если вы мужчина, f если вы женщина\n");
    printf("или q если вы хотите выйти из прогаммы:\n");
    ch = get_first();
    while (ch != 'f' && ch != 'm' && ch != 'q') {
        printf("Выбирайте m, f или q.\n");
        ch = get_first();
    }
    return ch;
}

void calculatorM(void) { // функция для расчета идеаллного веса мужчины 
    float iwm = 0.0f;
    float excesw = 0.0f;
    float mayor = 0.0f;
    printf("Рост: ");
    scanf("%f", &stature);
    printf("Вес: ");
    scanf("%f", &weight);
    if (stature >= 150) {
        mayor = stature - 150;
        iwm = 48 + (1.1 * mayor);
        excesw = weight - iwm;
        if (excesw == 0) {
            printf("Поздравляем, у Вас идеальный вес!");
        } else
            if (weight > iwm) {
            printf("вам рекомендуем похудеть на %.1f Кг!\n", excesw);
        } else
            printf("вам рекомендуем потолстеть на %.1f Кг!\n", (-1) * excesw);
    } else
        // if (stature < 150) {
        printf("Извините, расчет идеального веса верный только для мужчин с ростом выше 150см\n");
}

void calculatorF(void) { // функция для расчета идеаллного веса женщины
    float iwf = 0.0f;
    float excesw = 0.0f;
    float mayor = 0.0f;
    printf("Рост: ");
    scanf("%f", &stature);
    printf("Вес: ");
    scanf("%f", &weight);
    if (stature >= 150) {
        mayor = stature - 150;
        iwf = 45 + (0.9 * mayor);
        excesw = weight - iwf;
        if (excesw == 0) {
            printf(" Поздравляем, у Вас идеальный вес!");
        } else
            if (weight > iwf) {
            printf("Вам рекомендуем похудеть на %.1f Кг!\n", excesw);
        } else
            printf("Вам рекомендуем потолстеть на %.1f Кг!\n", (-1) * excesw);

    } else
        printf("Извините, расчет идеального веса верный только для мужчин с ростом выше 150см\n");
}