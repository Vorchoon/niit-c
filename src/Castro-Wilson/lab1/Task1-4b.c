/*Программа , которая переводит рост из американских системы (футы, дюймы) в
европейскую (сантиметры).
 by Wilson Castro. 10.2016
 */
#include <stdio.h>
#include <string.h>
#define  FOOTINCM 30.48f
#define  INCHINCM  2.54f
int Foot = 0;
int Inch = 0;
float statureCent = 0.0f;
char name[256];
void translateTocm(void);

int main(void) {
    printf("Вводите ваше имя\n");
    scanf("%s", name);
    printf("Привет %s, пожалуйста, выбирайте ваш рост в американских единицах\n", name);
    scanf("%d,%d", &Foot, &Inch);
    translateTocm();
    return 0;
}

void translateTocm(void) {  // функция для перевода футов на сантимеры 
    if (Inch * 0.1 >= 0.1 && Inch * 0.1 <= 0.9) {  // условия в случае, того что пользователь ведет 1 или 2 десятичные
        statureCent = (Foot * (FOOTINCM))+((Inch * 0.1)*FOOTINCM);
    } else
        statureCent = (Foot * (FOOTINCM))+((Inch * 0.01)*FOOTINCM);
    printf("%s ваш рост в см %.2fсм\n", name, statureCent);
}