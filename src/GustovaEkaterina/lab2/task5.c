//Write a program that displays the 10 passwords

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#define NumPasswords 10
#define LenPasswords 8

void InputPasswords(int flag);
void InputOnePass(int flag);

int main()
{
    srand (time(NULL));
	int countfornum = 0;
	InputPasswords(countfornum);
	return 0;
}
void InputPasswords(int flag)
{
	int countforlen = LenPasswords;
	InputOnePass(countforlen);
	putchar('\n');
	flag++;
	if (flag < NumPasswords)
		InputPasswords(flag);
}
void InputOnePass(int flag)
{
	char buf = rand() % 122;
	if (isalnum(buf))
	{
		putchar(buf);
		flag--;
	}
	if (flag)
		InputOnePass(flag);
}