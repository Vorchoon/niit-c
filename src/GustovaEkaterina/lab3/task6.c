/* Write a program that creates an integer array of size N,
and then finds the sum of the elements between the minimum and maximum elements.*/

#define _CRT_SECURE_NO_WARNINGS_
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <time.h>

#define SIZE 80
#define border 10

void GetLane(int *lane);
void FindMaxAndMin(int *lane);
void FindSum(int *lane, int border1, int  border2);

int main()
{
	int randomlane[SIZE];
	srand(time(NULL));
	GetLane(randomlane);
	FindMaxAndMin(randomlane);
	return 0;
}
void GetLane(int *lane)
{
	int i = 0;
	for (i = 0; i < SIZE; i++)
	{
		lane[i] = 0 + rand() % border;
	}
}
void FindMaxAndMin(int *lane)
{
	int max = SIZE - 1;
	int min = SIZE - 1;
	int i = 0;
	while (1)
	{
		if (lane[i] > lane[max])
			max = i;
		else if (lane[i] < lane[min])
			min = i;
		i++;
		if (i = SIZE)
			break;
	}
	FindSum(lane, max, min);
}
void FindSum(int *lane, int border1, int  border2)
{
	int sum = 0;
	if (border1 > border2)
		while (border1 > border2)
		{
			border2++;
			sum += lane[border2];
		}
	else if(border1 < border2)
		while (border2 > border1)
		{
			border1++;
			sum += lane[border1];
		}
	printf("%d", sum);
}