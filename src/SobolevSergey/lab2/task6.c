/* Практикум 2. Задача 6.
Написать программу, очищающую строку от лишних пробелов. Лишними счи-
таются пробелы в начале строки, в конце строки и пробелы между словами,
если их количество больше 1.
Замечание:
В данной программе запрещёно создавать дополнительные массивы, то есть
необходимо стремиться к экономии памяти. Время выполнения программы зна-
чения не имеет.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 80

int main()
{	
	char string[N];
	char *pointer;

	printf("INPUT string (max 79 characters):"); 
	fgets(string,N,stdin);

	do
	{
		pointer=strstr(string,"  ");
		
		if(pointer==NULL)
			break;
		
		strcpy(pointer,pointer+1);
	} 
	while(1);
	
	printf("OUTPUT:%s\n", string);

	return 0;
}