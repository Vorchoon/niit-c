/* Практикум №3. Задача 2.
Написать программу, которая для введённой строки определяет ко-
личество слов и выводит каждое слово на отдельной строке и его
длину
Замечание:
Слова разделяются любым количеством пробелов.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 256

int main( )
{
	char string[N];
	int i,count=0;

	puts("Enter a string:");
	fgets(string,N,stdin);
	string[strlen(string)-1]=0;

	for(i=0;i<strlen(string)-1;i++)
		if((string[i]==' ')&&(string[i+1]!=' '))
			count++;

	if(string[0]!=' ')
		count++;

	printf("The number of words in the string - %d\n",count);

	for(i=0, count=0;string[i];i++)
	{
		if(string[i]!=' ')
		{
			putchar(string[i]);
			count++;
		}
		else if(string[i]==' ' || string[i]=='\0')
		{
			if(count!=0)
				printf(" - %d \n",count);
			
			count=0;
		}
	}

	if(count!=0)
		printf(" - %d \n",count);

	putchar('\n');

	return 0;
}