/* Практикум 1. Задача 4. 
Написать программу, которая переводит рост из американской си-
стемы (футы, дюймы) в европейскую (сантиметры). Данные вво-
дятся в виде двух целых чисел, выводятся в виде вещественного
числа с точностью до 1 знака. 1 фут = 12 дюймов. 1 дюйм = 2.54
см.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void cleanIn() 
{
    char c;
    do 
	{
        c = getchar();
	} 
	while (c != '\n' && c != EOF);
}
int main()
{
	int feet, inches;
	float cm;

    while (1)
	{
        puts("Enter values of feet and inches in the following format (10 10):");
        if (scanf("%d%d", &feet, &inches) == 2 && (feet >= 0 && inches >= 0))
            break;
        else
		{
            puts("Input error!");
            cleanIn();
        }
    }


	if (feet != 0)
		cm=((feet * 12.0)+inches) * 2.54f;
	else
		cm=inches * 2.54f;
	printf("%.1f cm\n",cm);
	return 0;
}