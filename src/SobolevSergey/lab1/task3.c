/* Практикум 1. Задача 3.
3. Написать программу, которая переводит значение угла из граду-
сов в радианы, и, наоборот, в зависимости от символа при вводе.
Например:
45.00D
- означает значение в градусах, а
45.00R
- в
радианах. Ввод данных осуществляется по шаблону
%f%c
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void cleanIn() 
{
    char c;
    do 
	{
        c = getchar();
	} 
	while (c != '\n' && c != EOF);
}

int main()
{
	float value, result;
	char c;
    while (1) 
	{
        printf("Enter the value of an angle in the following format (10.00D/10.00R):\n");
        if (scanf("%f%c", &value, &c) == 2 && (c == 'D' || c == 'R'))
            break;
        else 
		{
            puts("Input error!");
            cleanIn();
        }
    }
	if (c == 'R') 
	{
		result = (180.0 / 3.14159f) * value;
		printf("%fD\n", result);
	}
	else if (c == 'D') 
	{
		result = (3.14159f / 180) * value;
		printf("%fR\n", result);
	}
	return 0;
}