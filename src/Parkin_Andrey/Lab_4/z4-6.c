/*6. �������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������:
����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������.*/
#include<stdio.h>
#define NAME_LEN 256
#define MAX_CNT 100

int main()
{
	char name[MAX_CNT][NAME_LEN];
	int min_age = 1000, max_age = -1, age, cnt, i=0;
	char *young = NULL;
	char *old = NULL;

	printf("Please enter the number of relatives in the family (from 1 to %d): ", MAX_CNT);
	scanf("%d", &cnt);
	if(cnt < 1 || cnt > MAX_CNT)
	{
		printf("Invalid number of relatives\n");
		return 1;
	}
	printf("\n");
	for(i=0; i<cnt; i++)
	{
		printf("%d. Please, enter name and age (name(without spaces) age): ", i+1);
		scanf("%s %d", name[i], &age);
		if(age > max_age)
		{
			old = name[i];
			max_age = age;
		}
		if(age < min_age)
		{
			young = name[i];
			min_age = age;
		}
	}
	if(young != NULL)
		printf("Young: %s - %d years\n", young, min_age);
	if(old != NULL)
		printf("Old: %s - %d years\n", old, max_age);
	return 0;
}