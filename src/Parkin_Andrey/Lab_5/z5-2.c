/*2. �������� ��������� ������������, ��������� �� ����� ������-
�����, ������������ �� ����������� ������������� ��������� �*�.
����������� ����������� � ��������� ���������� �������, � ��-
��� ��� ����� � ����������� ���������� � ��������� ��� �����.
���������:
������� ������ ��������� � ���� ��������� ������������������ �����:
1) ������� ������� (���������� ���������)
2) ������������ ��������� ������� �������� ������ ��������� (��������� �*�)
3) ����������� �������� � ������ ��������� �������
4) ������� ������
5) ����� ������� �� ����� (���������)
6) ��������� ��������
7) ������� � ���� 1.*/
#include<stdio.h>
#include <stdlib.h>
#include<time.h>
#include <windows.h>

#define N 20
#define REPEAT_CNT 20

void CleanKldArr(char (*p_kld_arr)[N])
{
	int i, j;
	for(i=0; i<N; i++)
		for(j=0; j<N; j++)
			p_kld_arr[i][j] = ' ';
}

void FillQuarterKld(char (*p_kld_arr)[N])
{
	int i, j, rnd;
	for(i=0; i<N/2; i++)
	{
		for(j=0; j<N/2; j++)
		{
			rnd = 0 + rand()%2;
			if(rnd == 1)
				p_kld_arr[i][j] = '*';
		}
	}
}

void CopyQuarterKld(char (*p_kld_arr)[N])
{
	int i, j;
	// ������� ������ ������
	for(i=0; i<N/2; i++)
		for(j=0; j<N/2; j++)
			if(p_kld_arr[i][j] != ' ')
				p_kld_arr[i][N-1-j] = '*';
	// ������ �������
	for(i=0; i<N/2; i++)
		for(j=0; j<N; j++)
			if(p_kld_arr[i][j] != ' ')
				p_kld_arr[N-1-i][j] = '*';
}

void PrintKld(char (*p_kld_arr)[N])
{
	int i, j;
	for(i=0; i<N; i++)
	{
		for(j=0; j<N; j++)
			printf("%c",p_kld_arr[i][j]);
		printf("\n");
	}
}
int main()
{
	char kld_arr[N][N];
	int i;
	srand(time(NULL));

	for(i=0; i<REPEAT_CNT; i++)
	{
		CleanKldArr(kld_arr);
		FillQuarterKld(kld_arr);
		CopyQuarterKld(kld_arr);
		PrintKld(kld_arr);
		Sleep(500);
		system("cls");
	}
	return 0;
}