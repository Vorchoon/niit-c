/*4. �������� ���������, ������� ������ ��������� ��������� ���� �
������������ �������� ����� � ������ ������.
���������: ��������� ��������� ������������ ���������� ���� � ������
��� ���������. ��� ������ ������ ���������� �������, ������������� � 
������ ������ 1.
1. �������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
���������:
��������� ������ �������� ������� �� ���� �������:
a) printWord - ������� ����� �� ������ (�� ����� ������ ��� �������);
b) getWords - ��������� ������ ���������� �������� ������ ���� ����;
c) main - �������� �������.*/
#include <stdio.h>
#include <time.h>

#define N 256

int GetWord(char *p_ent, char **pp_words)
{
	int cnt_word = 0;
	while(*p_ent)
	{
		if(*p_ent != ' ' && *p_ent != '\n' && *p_ent != '\t' && *p_ent != '\0')
		{
			cnt_word++;
			*pp_words = p_ent;
			pp_words++;
			while(*p_ent != ' ' && *p_ent != '\n' && *p_ent != '\t' && *p_ent != '\0')
				p_ent++;
		}
		else
			p_ent++;
	}
	return cnt_word;
}

void MixWords(char **pp_words, int w_cnt)
{
	int i, rand_num;
	char *delta;
	for(i=0; i<w_cnt; i++)
	{
		rand_num = i + rand()%(w_cnt - i);
		delta = pp_words[i];
		pp_words[i] = pp_words[rand_num];
		pp_words[rand_num] = delta;
	}
}

void PrintWords(FILE *f_out, char **p_word, int cnt)
{
	int i;
	for(i = 0; i < cnt; i++)
	{
		while(*p_word[i] != ' ' && *p_word[i] != '\n' && *p_word[i] != '\t' && *p_word[i] != '\0')
		{
			putc(*p_word[i], f_out);
			p_word[i]++;
		}
		putc(' ', f_out);
	}
	putc('\n', f_out);
}


int main()
{
	char str[N]; 
	char *p_words[N];
	int i, cnt_words;
	FILE *f_in, *f_out;
	srand(time(NULL));

	f_in = fopen("input.txt", "r");
	f_out = fopen("output.txt", "w");
	if (f_in == NULL || f_out == NULL)
	{
		perror("File:");
		return 1;
	}
	while(!feof(f_in))
	{
		fgets(str, N, f_in);
		cnt_words = GetWord(str, p_words);
		MixWords(p_words, cnt_words);
		PrintWords(f_out, p_words, cnt_words);
	}
	fclose(f_in);
	fclose(f_out);
	return 0;
}
