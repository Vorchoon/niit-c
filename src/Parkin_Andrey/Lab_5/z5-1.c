/*1. �������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
���������:
��������� ������ �������� ������� �� ���� �������:
a) printWord - ������� ����� �� ������ (�� ����� ������ ��� �������);
b) getWords - ��������� ������ ���������� �������� ������ ���� ����;
c) main - �������� �������.*/
#include<stdio.h>
#include<time.h>

#define N 256
#define M 100

void GetWords(char *p_ent, char **pp_words, int *p_cnt_words)
{
	while(*p_ent)
	{
		if(*p_ent != ' ' && *p_ent != '\n' && *p_ent != '\t' && *p_ent != '\0')
		{
			(*p_cnt_words)++;
			*pp_words = p_ent;
			pp_words++;
			while(*p_ent != ' ' && *p_ent != '\n' && *p_ent != '\t' && *p_ent != '\0')
				p_ent++;
		}
		else
			p_ent++;
	}
}
void MixWords(char **pp_words, int w_cnt)
{
	int i, rand_num;
	char *delta;
	for(i=0; i<w_cnt; i++)
	{
		rand_num = i + rand()%(w_cnt - i);
		delta = pp_words[i];
		pp_words[i] = pp_words[rand_num];
		pp_words[rand_num] = delta;
	}
}

void PrintWord(char *p_word, int w_num)
{
	while(*p_word != ' ' && *p_word != '\n' && *p_word != '\t' && *p_word != '\0')
		printf("%c", *p_word++);
	printf(" ");
}

int main()
{
	char ent_str[N];
	char *p_words[M];
	int i, cnt_words = 0;
	srand(time(NULL));
	printf("Please, enter string (max length %d)\n", N);
	fgets(ent_str, N, stdin);
	GetWords(ent_str, p_words, &cnt_words);
	MixWords(p_words, cnt_words);
	for(i=0; i<cnt_words; i++)
		PrintWord(p_words[i], i);
	printf("\n");
	return 0;
}
