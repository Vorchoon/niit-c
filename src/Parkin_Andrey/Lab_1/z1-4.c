/*4. �������� ���������, ������� ��������� ���� �� ������������ ��-
����� (����, �����) � ����������� (����������). ������ ���-
����� � ���� ���� ����� �����, ��������� � ���� �������������
����� � ��������� �� 1 �����. 1 ��� = 12 ������. 1 ���� = 2.54
��.*/

#include<stdio.h>
#define INCH_to_CM 2.54
#define FT_to_CM 12 * INCH_to_CM

int main()
{
	int ft, inch;
	float cm;
	while(1)
	{
		printf("Please, enter your growht in american format (for example 6' 5\")\n");
		scanf("%d' %d\"", &ft, &inch);
		if ((ft>12 || ft<0) || (inch>12 ||inch<0))
			printf("Something wrong (foots: range from 0 to 12 and inches: range from 0 to 12)\n");
		else
			break;
	}
	cm = ft * FT_to_CM + inch * INCH_to_CM;
	printf("Your growht in centimeters is %.1f\n", cm);
	return 0;
}
