/*3. �������� ���������, ������� ������ ������� ������������� �������� ���
������������� �����, ��� �������� ������� � ��������� ������. ��������� ������
�������� �� ����� ������� �������������, ��������������� �� �������� �������.
	���������: � ��������� ���������� ���������� ����������� ��� SYM, � �������
����� ������� ��� ������� � ������� ������������� (������������ ����� �� 0 �� 1).
����� ������� �����, ������ �������� SYM ������ ���� ������������ �� �������.*/
#include<stdio.h>

#define ASCII_CNT 256

struct SYM
{
	unsigned char ascii; //ASCII-��� �������
	float freq;//������� ������������� ������� �� 0.0 �� 1.0
} tmp;

//�������� ������� ��������
struct SYM AddSym(int ascii, float freq)
{
	struct SYM new_sym;
	new_sym.ascii = ascii;//ASCII-��� �������
	new_sym.freq = freq;//������� ������������� ������� �� 0.0 �� 1.0
	return new_sym;
}

int main(int argc, char* argv[])
{
	int i, j = 0;//��������
	int ch_fp;//����������, � ������� �������� ������� �� �����
	int ascii[ASCII_CNT] = {0};// ������ ������������� �������� �� ASCII-�����
	int uniq_cnt_sym = 0;//���������� ���������� �������� � �����
	int all_cnt_ch = 0;//����� ���������� ��������(������) � ����� (������ �����)
	struct SYM sym_arr[ASCII_CNT];//������ ��������
	FILE *fp = fopen(argv[1], "r");
	if(fp == NULL)
	{
		printf("File error-horror! I can't open file.  :(\n");
		return 1;
	}
	//������ �������� ���� � ��������� ������ ������������� (�� ���� ���������� �����)
	while((ch_fp = fgetc(fp)) != -1)
	{
		ascii[ch_fp]++;
		all_cnt_ch++;
	}
	//����� ���������� ���������� ��������
	for(i = 0; i < ASCII_CNT; i ++)
		if(ascii[i] != 0)
			uniq_cnt_sym++;
	printf("Unique symbols %d in file.\n", uniq_cnt_sym);
	//����������� ������� �������������
	if(uniq_cnt_sym > 0)
	{
		for(i = 0; i < ASCII_CNT; i ++)
		{
			if(ascii[i] != 0)//��������� ������ � �� ������� ��������������
				sym_arr[j++] = AddSym(i, (float)ascii[i]/(float)all_cnt_ch);//j++������������� � ���������� �������� ���������
		}
	}
	//���������� �� �������� �������������
	for(i = 1;  i < uniq_cnt_sym; i++)
	{
		for(j = i-1; (j > 0 && sym_arr[j+1].freq > sym_arr[j].freq); j--)
		{
			tmp = sym_arr[j+1];
			sym_arr[j+1] = sym_arr[j];
			sym_arr[j] = tmp;
		}
	}
	//������ ������� ��������
	for(i = 0;  i < uniq_cnt_sym; i++)
		printf("%03d. '%c' - %f\n", i, sym_arr[i].ascii, sym_arr[i].freq);
	fclose(fp);
	return 0;
}