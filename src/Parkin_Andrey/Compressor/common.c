#include "common.h"

//������� ������� �� ������ ������
void LeftTrimStr(char* str)
{
	int spaces_len = strspn(str, " ");
	int str_len = strlen(str);
	int i;
	if(spaces_len > 0)
	{
		for(i = spaces_len; i < str_len; i++)
			str[i - spaces_len] = str[i];
		str[str_len-spaces_len] = '\0';
	}
}

int StrToInt(char* str)
{
	int i, result = 0, len = strlen(str);
	for(i=0; i<len; i++)
		result += (str[i] - '0')*pow(10, len-i-1);
	return result;
}

unsigned long GetFileSize(char* file_name)
{
	FILE*  fp = fopen(file_name,"r");
	fseek(fp,0,SEEK_END);
	return ftell(fp); 
}

void GenerateSymFreqFromFILE(char* file_name, int* ascii, int* unique_sym_cnt, int* all_sym_cnt)
{
	FILE* fp;
	int ch_ascii, i;
	fp = fopen(file_name, "r");
	if(fp == NULL)
	{
		printf("File horror-error!!!\n");
		return 1;
	}
	while((ch_ascii = fgetc(fp)) != -1)
	{
		ascii[ch_ascii]++;
		(*all_sym_cnt)++;
	}
	for(i = 0; i < ASCII_CNT; i++)
		if(ascii[i] != 0)
			(*unique_sym_cnt)++;
	fclose(fp);
}

struct SYM* AddNewSym(int ascii, float freq)
{
	struct SYM* new_sym =  malloc(sizeof(struct SYM));//��������� ������ ��� ������� ���������
	new_sym->ascii = ascii;//ASCII-��� ��������
	new_sym->freq = freq;//������� ������������� �������
	new_sym->left = NULL;
	new_sym->right = NULL;
	new_sym->new_code[0] = 0;
	return new_sym;
}

void SortSymArr(struct SYM* sym_arr[], int cnt)
{
	int i, j;
	struct SYM* tmp;
	for(i = 1; i < cnt; i++)
	{
		for(j = i-1; (j>=0 && sym_arr[j+1]->freq > sym_arr[j]->freq); j--)
		{
			tmp = sym_arr[j+1];
			sym_arr[j+1] = sym_arr[j];
			sym_arr[j] = tmp;
		}
	}
}

void PrintSYM(struct SYM* sym, char* pref, int ind)
{
	printf("%d %s: '%c'(%d) - %f , code = '%s'", ind, pref, sym->ascii, sym->ascii, sym->freq, sym->new_code);
	if(sym->left)
		printf(" left = '%c' (%d)", sym->left->ascii, sym->left->ascii);
	if(sym->right)
		printf(" right = '%c' (%d)", sym->right->ascii, sym->right->ascii);
	printf("\n");
}

struct SYM* BuildTree(struct SYM* sym_arr[], int N, int* sym_cnt)
{
	int i;
	struct SYM* tmp;
	struct SYM* node = malloc(sizeof(struct SYM));//������� ���������
	node->freq = sym_arr[N-1]->freq + sym_arr[N-2]->freq;// ����� ������ ���������� � �������������� ���������
	node->left = sym_arr[N-1];//����� �������
	node->right = sym_arr[N-2];//������ �������
	node->new_code[0] = 0;
	node->ascii = 0;

	//�������� ��������� ���� � ������
	(*sym_cnt)++; 
	sym_arr[(*sym_cnt)-1] = node;
	for(i=(*sym_cnt)-2; (i>=0 && sym_arr[i+1]->freq > sym_arr[i]->freq); i--)
	{
		tmp = sym_arr[i+1];
		sym_arr[i+1] = sym_arr[i];
		sym_arr[i] = tmp;
	}

	if(N == 2)
		return node;
	return BuildTree(sym_arr, N-1, sym_cnt);
}

void MakeCodes(struct SYM* root)
{
	if(root->left)
	{
		strcpy(root->left->new_code, root->new_code);
		strcat(root->left->new_code, "0");
		MakeCodes(root->left);
	}
	if(root->right)
	{
		strcpy(root->right->new_code, root->new_code);
		strcat(root->right->new_code, "1");
		MakeCodes(root->right);
	}
}

int GenerateCompressTmpBinaryFile(char* file_name, struct SYM* sym_arr[], int sym_len)
{
	FILE *fp, *fp_tmp;
	unsigned char ch_ascii, i, tmp_ch_cnt=0, tail = 0;
	fp = fopen(file_name, "r");
	fp_tmp = fopen(TMP_BINARY_FILE, "w+");
	if(fp == NULL || fp_tmp == NULL)
	{
		printf("File horror-error!!!\n");
		return 1;
	}
	while(!feof(fp))
	{
		ch_ascii = fgetc(fp);
		for(i = 0; i < sym_len; i++)
		{
			if(sym_arr[i]->ascii == ch_ascii)
			{
				fputs(sym_arr[i]->new_code, fp_tmp);
				tmp_ch_cnt+=strlen(sym_arr[i]->new_code);
				break;
			}
		}
	}
	fclose(fp);
	fclose(fp_tmp);
	tail = 8 - tmp_ch_cnt%8;
	if(tail == 8)
		return 0;
	return tail;
}

unsigned char Pack(unsigned char buf[])
{
	union CODE code;
	code.byte.b1 = buf[0] - '0';
	code.byte.b2 = buf[1] - '0';
	code.byte.b3 = buf[2] - '0';
	code.byte.b4 = buf[3] - '0';
	code.byte.b5 = buf[4] - '0';
	code.byte.b6 = buf[5] - '0';
	code.byte.b7 = buf[6] - '0';
	code.byte.b8 = buf[7] - '0';
	return code.ch;
}

void GenerateCompressedFileName(char* file_name, char* file_ext, char* cps_file_name)
{
	int file_name_size = strcspn(file_name, ".");
	// �������� �������� ����� ��� ����������
	strncpy (cps_file_name, file_name, file_name_size);
    cps_file_name[file_name_size] = '\0';
	strcat(cps_file_name, ".cps");
	// �������� ����������
	strcpy(file_ext, &file_name[file_name_size+1]);
	
	printf("new file name = %s\n", cps_file_name);
	printf("base file extension = %s\n", file_ext);
}

void FillCompressedDate(char* cps_file_name)
{
	FILE *fp_tmp, *fp_cps;
	int i, buf_size;
	unsigned char buf[9], cps_ch;
	fp_cps = fopen(cps_file_name, "ab");
	fp_tmp = fopen(TMP_BINARY_FILE, "r");
	if(fp_cps == NULL || fp_tmp == NULL)
	{
		printf("File horror-error!!!\n");
		return 1;
	}
	while((buf_size = fread(buf, sizeof(char), 8, fp_tmp)) > 0)
	{
		if(buf_size < 8)
			for(i = buf_size; i < 8; i++)
				buf[i] = '0';
		buf[8] = '\0';
		cps_ch = Pack(buf);
		fputc(cps_ch, fp_cps);
		if(buf_size < 8)
			break;
	}
	fclose(fp_cps);
	fclose(fp_tmp);
}

void FillCompressedHeader(char* cps_file_name, int unique_sym_cnt, struct SYM* sym_arr[], int sym_len, int tail, int file_size, char* file_ext)
{	
	FILE *fp_cps;
	int i;
	fp_cps = fopen(cps_file_name, "wb");
	if(fp_cps == NULL)
	{
		printf("File horror-error!!!\n");
		return 1;
	}

	//���������
	fprintf(fp_cps, "%*s", HEADER_SIGNATURE_LEN, HEADER_SIGNATURE);
	//����� ���������� ��������
	fprintf(fp_cps, "%0*d", HEADER_UNIQUE_LEN, unique_sym_cnt);
	//������� �������������
	for(i=0; i<sym_len; i++)
	{
		if(sym_arr[i]->ascii > 0)
		{
			fputc(sym_arr[i]->ascii, fp_cps);
			fwrite(&sym_arr[i]->freq, sizeof(float), 1, fp_cps);
		}
	}
	//����� ������
	fprintf(fp_cps, "%0*d", HEADER_TAIL_LEN, tail);
	//������ ��������� �����
	fprintf(fp_cps, "%0*d", HEADER_FILESIZE_LEN, file_size);
	//����������
	fprintf(fp_cps, "%*s", HEADER_FILEEXT_LEN, file_ext);
	fclose(fp_cps);
}

int ReadHeaderFromFile(char* cps_file_name, struct SYM* sym_arr[], int* unique_sym_cnt, int* all_sym_cnt, int* tail, char* base_file_ext)
{
	char buf[50];
	int i, header_len=0;
	unsigned char ch;
	float ch_freq;
	FILE* fp;

	fp = fopen(cps_file_name, "rb");
	if(fp == NULL)
	{
		printf("File horror-error!!!\n");
		return 1;
	}
	//��������� ���������
	fread(buf, sizeof(char), HEADER_SIGNATURE_LEN, fp);
	buf[HEADER_SIGNATURE_LEN] = '\0';
	header_len += HEADER_SIGNATURE_LEN;
	LeftTrimStr(buf);
	if(strcmp(buf, HEADER_SIGNATURE) != 0)
	{
		printf("Invalid signature.\n");
		return -1;
	}
	//��������� ���������� ���������� ��������
	fread(buf, sizeof(char), HEADER_UNIQUE_LEN, fp);
	buf[HEADER_UNIQUE_LEN] = '\0';
	header_len += HEADER_UNIQUE_LEN;
	*unique_sym_cnt = StrToInt(buf);
	if(*unique_sym_cnt <= 0)
	{
		printf("Invalid unique symbol count.\n");
		return -1;
	}
	printf("unique symbol count - %s - %d\n", buf, *unique_sym_cnt);
	//��������� ������� �������������
	for(i=0; i<*unique_sym_cnt; i++)
	{
		ch = fgetc(fp);
		if( fread(&ch_freq, sizeof(float), 1, fp) < 1) {
			printf("read char '%c' freq error\n", ch);
			return -1;
		}
		header_len += 1 + HEADER_FREQ_LEN;
		sym_arr[i] = AddNewSym(ch, ch_freq);
	}
	//�����
	fread(buf, sizeof(char), HEADER_TAIL_LEN, fp);
	buf[HEADER_TAIL_LEN] = '\0';
	header_len += HEADER_TAIL_LEN;
	*tail = StrToInt(buf);
	printf("tail - %s - %d\n", buf, *tail);

	//������ �����
	fread(buf, sizeof(char), HEADER_FILESIZE_LEN, fp);
	buf[HEADER_FILESIZE_LEN] = '\0';
	header_len += HEADER_FILESIZE_LEN;
	*all_sym_cnt = StrToInt(buf);
	printf("all_sym_cnt - %s - %d\n", buf, *all_sym_cnt);

	//��������� ���������� ��������� �����
	fread(base_file_ext, sizeof(char), HEADER_FILEEXT_LEN, fp);
	base_file_ext[HEADER_FILEEXT_LEN] = '\0';
	header_len += HEADER_FILEEXT_LEN;
	LeftTrimStr(base_file_ext);
	printf("base_file_ext - %s \n", base_file_ext);

	fclose(fp);
	return header_len;
}

void GenerateDeCompressTmpBinaryFile(char* cps_file_name, int header_len, int tail)
{
	int i, tmp_ch_cnt=0, buf_len=8;
	unsigned char buf[9], ch_ascii;
	unsigned long f_size = 0, cur_size = 0;
	FILE *fp, *fp_tmp;
	f_size = GetFileSize(cps_file_name);
	fp = fopen(cps_file_name, "rb");
	fp_tmp = fopen(TMP_BINARY_FILE, "w+");
	if(fp == NULL || fp_tmp == NULL)
	{
		printf("File horror-error!!!\n");
		return 1;
	}
	fseek(fp , header_len, SEEK_SET);
	cur_size += header_len;
	while(cur_size < f_size) 
	{
		ch_ascii = fgetc(fp);
		cur_size++;
		DePack(ch_ascii, buf);
		if(cur_size == f_size) buf_len -= tail;
		fwrite(buf, sizeof(char), buf_len, fp_tmp);
	}

	fclose(fp);
	fclose(fp_tmp);
}

void DePack(char ch, char* buf)
{
	union CODE code;
	int i;
	code.ch = ch;
	buf[0] = code.byte.b1 + '0';
	buf[1] = code.byte.b2 + '0';
	buf[2] = code.byte.b3 + '0';
	buf[3] = code.byte.b4 + '0';
	buf[4] = code.byte.b5 + '0';
	buf[5] = code.byte.b6 + '0';
	buf[6] = code.byte.b7 + '0';
	buf[7] = code.byte.b8 + '0';
	buf[8] = '\0';
}

void GenerateBaseFileName(char* cps_file_name, char* base_file_ext, char* base_file_name)
{
	int file_name_size = strcspn(cps_file_name, ".");
	// �������� �������� ����� ��� ����������
	strncpy (base_file_name, cps_file_name, file_name_size);
    base_file_name[file_name_size] = '\0';
	strcat(base_file_name, "_new.");
    base_file_name[file_name_size+5] = '\0';
	strcat(base_file_name, base_file_ext);
	
	printf("base file name = %s\n", base_file_name);
}

void GenerateBaseFile(char* base_file_name, struct SYM* sym_arr[], int sym_len)
{
	FILE *fp_tmp, *fp_base;
	int i, buf_size;
	char buf[20];
	char ch;
	fp_base = fopen(base_file_name, "w+");
	fp_tmp = fopen(TMP_BINARY_FILE, "r");
	if(fp_base == NULL || fp_tmp == NULL)
	{
		printf("File horror-error!!!\n");
		return 1;
	}
	while(1)
	{
		buf_size = 0;
		ch = GetCharFromTmp(fp_tmp, sym_arr[0], buf, &buf_size);
		printf("%c", ch);
		if(ch == -1)
			break;
		fputc(ch, fp_base);
	}
	printf("\n");
	fclose(fp_base);
	fclose(fp_tmp);
}

char GetCharFromTmp(FILE* fp, struct SYM* tree_node, char* buf, int* buf_size)
{
	char ch;
	if(tree_node->left == NULL && tree_node->right == NULL)
	{
		if(strcmp(buf, tree_node->new_code) == 0)
			return tree_node->ascii;
		return -1;
	}
	ch = fgetc(fp);
	buf[*buf_size] = ch;
	(*buf_size)++;
	buf[*buf_size] = '\0';
	if(ch == -1)
		return -1;
	if(ch == '1' && tree_node->right)
		return GetCharFromTmp(fp, tree_node->right, buf, buf_size);
	if(ch == '0' && tree_node->left)
		return GetCharFromTmp(fp, tree_node->left, buf, buf_size);
	return -1;
}