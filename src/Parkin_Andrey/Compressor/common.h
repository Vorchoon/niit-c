#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<math.h>

#define ASCII_CNT 256
#define TMP_BINARY_FILE "tmp.101"

#define HEADER_SIGNATURE "NIIT"
#define HEADER_SIGNATURE_LEN 4
#define HEADER_UNIQUE_LEN 3
#define HEADER_FREQ_LEN 4
#define HEADER_TAIL_LEN 1
#define HEADER_FILESIZE_LEN 10
#define HEADER_FILEEXT_LEN 5

struct SYM
{
	unsigned char ascii; // ASCII-��� �������
	float freq;//������� ������������� �� 0.000 �� 1.000
	char new_code[ASCII_CNT];//������ � ����� �������������� ������� �� 1 � 0
	struct SYM* left;
	struct SYM* right;
};

union CODE
{
	unsigned char ch;
	struct
	{
		unsigned short b1:1;
		unsigned short b2:1;
		unsigned short b3:1;
		unsigned short b4:1;
		unsigned short b5:1;
		unsigned short b6:1;
		unsigned short b7:1;
		unsigned short b8:1;
	} byte;
};

//����� �������
void LeftTrimStr(char* str);
int StrToInt(char* str);
struct SYM* AddNewSym(int ascii, float freq);
void SortSymArr(struct SYM* sym_arr[], int cnt);
struct SYM* BuildTree(struct SYM* sym_arr[], int N, int* sym_cnt);
void PrintSYM(struct SYM* sym, char* pref, int ind);
void MakeCodes(struct SYM* root);
unsigned long GetFileSize(char* file_name);

//����������
void GenerateSymFreqFromFILE(char* file_name, int* ascii, int* unique_sym_cnt, int* all_sym_cnt);
int GenerateCompressTmpBinaryFile(char* file_name, struct SYM* sym_arr[], int sym_len);
unsigned char Pack(unsigned char buf[]);
void GenerateCompressedFileName(char* file_name, char* file_ext, char* cps_file_name);
void FillCompressedDate(char* cps_file_name);
void FillCompressedHeader(char* cps_file_name, int unique_sym_cnt, struct SYM* sym_arr[], int sym_len, int tail, int file_size, char* file_ext);

//������������
int ReadHeaderFromFile(char* cps_file_name, struct SYM* sym_arr[], int* unique_sym_cnt, int* all_sym_cnt, int* tail, char* base_file_ext);
void GenerateDeCompressTmpBinaryFile(char* cps_file_name, int header_len, int tail);
void GenerateBaseFile(char* base_file_name, struct SYM* sym_arr[], int sym_len);
void GenerateBaseFileName(char* cps_file_name, char* base_file_ext, char* base_file_name);
void DePack(char ch, char* buf);
char GetCharFromTmp(FILE* fp, struct SYM* tree_node, char* buf, int* buf_size);