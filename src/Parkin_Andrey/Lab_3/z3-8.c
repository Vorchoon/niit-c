/*8. �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� ������ ��
�����. � ������ ������������� n ��������� ��������� �� ������. */
#include<stdio.h>
#include<string.h>
#define N 256
#define QTY_WORDS 100

int main()
{
	char user_str[N], user_word[N];
	int i=0, j=0, user_N=0, word_N=0;
	printf("Please, enter string (max length %d)\n", N);
	fgets(user_str, N, stdin);
	printf("Please, enter word number: ");
	while(1)
	{
		scanf("%d", &user_N);
		if(user_N < 1 || user_N > QTY_WORDS)
			printf("Try again (from 1 to %d)\n", QTY_WORDS);
		else
			break;
	}
	while(user_str[i]!='\0')
	{
		if(user_str[i]!=' ' && user_str[i]!='\t' && user_str[i]!='\n')
		{
			word_N++;
			j = 0;
			while(user_str[i]!=' ' && user_str[i]!='\t' && user_str[i]!='\n' && user_str[i]!='\0')
				user_word[j++] = user_str[i++];
			user_word[j] = '\0';
			if(word_N == user_N)
			{
				printf("Word %d is '%s'\n", user_N, user_word);
				break;
			}
		}
		else
			i++;
	} 
	if(word_N != user_N)
		printf("Word %d is not found\n", user_N);
	return 0;
}



