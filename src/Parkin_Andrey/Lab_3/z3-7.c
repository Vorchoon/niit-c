/*7. �������� ���������, ������� �������� ������� ������������� �������� ���
�������� ������, ��������������� �� �������� �������
���������: ������� ��������� ����� �������, ��� ������� ���� ����� ��������������
�������, � ����� �� ���� ��������.*/
#include<stdio.h>
#define SIZE 256

int main()
{
	char ent_arr[SIZE];
	int ascii_arr[SIZE] = {0}, frequences[SIZE/3];
	int ascii, i=0, j=0;
	printf("Please, enter string\n");
	fgets(ent_arr, SIZE, stdin);
	while(ent_arr[i] != '\0' && ent_arr[i] != '\n')
	{
		ascii = (int)ent_arr[i];
		ascii_arr[ascii]++;
		i++;
	}
	while(1)
	{
		frequences[j] = -1;
		for(i=0; i<SIZE; i++)
		{
			if(frequences[j] < ascii_arr[i])
			{
				if(j == 0 || ascii_arr[i] < frequences[j-1])
					frequences[j] = ascii_arr[i];
			}
		}
		if(frequences[j]<=0)
			break;
		j++;
	}
	i=0;
	while(frequences[i]>0)
	{
		for(j=0; j<SIZE; j++)
		{
			if(ascii_arr[j] == frequences[i])
				printf("'%c' - %d\n", (char)j, ascii_arr[j]);
		}
		i++;
	}
	return 0;
}