/*5. �������� ���������, ������� ��������� ������������� ������ ������� N, �
����� ������� ����� ���������, ������������� ����� ������ ����������-
��� � ��������� ������������� ����������.
���������: ������ ����������� ���������� �������: �������������� � ������������-
�� ������� (��� ����� �������...). */
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#define N 200

int main()
{
	int num_arr[N];
	int i, start_i=-1, end_i=-1, sum = 0;
	srand(time(NULL));
	for(i=0; i<N; i++)
	{
		num_arr[i] = -100 + rand()%N;
		if(num_arr[i] < 0 && start_i == -1)
			start_i = i;
		else if (num_arr[i] >= 0)
			end_i = i;
		printf("%d, ", num_arr[i]);
	}
	printf("\n");
	printf("Start number = %d(%d), End number = %d (%d)\n", start_i, 
		                   num_arr[start_i], end_i, num_arr[end_i]);
	if(start_i<end_i)
	{
		for(i = start_i; i <= end_i; i++)
			sum += num_arr[i];
	}
	printf("Summa = %d\n", sum);
	return 0;
}
