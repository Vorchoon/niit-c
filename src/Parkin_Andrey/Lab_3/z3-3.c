/* 3. �������� ���������, ������� ��� �������� ������ ������� ����� ������� ����� � ��� �����. */
#include<stdio.h>
#include<string.h>
#define N 256

int main()
{
	int max_len=0, i=0, j=0;
	char ent_arr[N], word[N], max_word[N];
	printf("Please, enter string (max length %d simbols)\n", N-1);
	fgets(ent_arr,256,stdin);
	while(ent_arr[i]!='\0')
	{
		if(ent_arr[i]!=' ' && ent_arr[i]!='\t' && ent_arr[i]!='\0' && ent_arr[i]!='\n')
		{
			j=0;
			do
			{
				word[j] = ent_arr[i];
				j++;
				i++;
			} while(ent_arr[i]!=' ' && ent_arr[i]!='\t' && ent_arr[i]!='\0' && ent_arr[i]!='\n');
			word[j] = '\0';
			if(max_len<j)
			{
				max_len=j;
				strcpy(max_word, word);
			}
		}
		else
			i++;
	}
	printf("Word with max length - %s (%d)\n", max_word, max_len);
	return 0;
}