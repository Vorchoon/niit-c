/*7. �������� ���������, ������� ������� ����� �� ���������.
���������:
(a) �������� ������� � ���� ���������� ����������� �������;
(b) ��������� ������� - � ������;
(c) ��������� ���������� (�� �������), ��� ��������� ������ ������� ������;
(d) ���� ������ ��������, ��������� ������������ � ������ ����� � ��
    �����������;
(e) ��������� ����� � ������ �� ��������� ��� ����������� ��� �������
    �������.
���������:
�������� �#� ���������� ����� ���������;
�������� ������ - ��������� ������;
�������� �X� - �������������� ��������.*/

#include<stdio.h>
#include<windows.h>
#define X_MAX 28
#define Y_MAX 16
#define CRNT_SYMBOL 'x' 
#define PREV_SYMBOL '.' 

void PrintLab(char (*lab)[X_MAX])
{
	int i, j;
	system("cls");
	for(i=0; i<Y_MAX; i++)
	{
		for(j=0; j<X_MAX; j++)
			printf("%c", lab[i][j]);
		printf("\n");
	}
	Sleep(30);
}

int SearchExit(char (*lab)[X_MAX], int y, int x)
{
	int result = 0;
	PrintLab(lab);
	// ���������, ��� ����� ����� �� ���������
	if(x == 0 || x == X_MAX-1 || y == 0 || y == Y_MAX-1)
		return 1;
	// ����������� �����
	if(result == 0 && lab[y-1][x] == ' ')
	{
		lab[y][x] = PREV_SYMBOL;
		lab[y-1][x] = CRNT_SYMBOL;
		result = SearchExit(lab, y-1, x);
	}
	// ��� ������
	if(result == 0 && lab[y][x+1] == ' ')
	{
		lab[y][x] = PREV_SYMBOL;
		lab[y][x+1] = CRNT_SYMBOL;
		result = SearchExit(lab, y, x+1);
	}
	// ���������� ����
	if(result == 0 && lab[y+1][x] == ' ')
	{
		lab[y][x] = PREV_SYMBOL;
		lab[y+1][x] = CRNT_SYMBOL;
		result = SearchExit(lab, y+1, x);
	}
	// ��� �����
	if(result == 0 && lab[y][x-1] == ' ')
	{
		lab[y][x] = PREV_SYMBOL;
		lab[y][x-1] = CRNT_SYMBOL;
		result = SearchExit(lab, y, x-1);
	}
	// �����
	if(result == 0)
	{
		lab[y][x] = '-';
		return 0;
	}
	// ����� ������
	return ++result;
}


int main() 
{
	int steps = 0;
	char labir[Y_MAX][X_MAX] = {
		"          #   #  #          ",
		"           #  # #           ",
		"        ############        ",
		"     ####          ####     ",
		"  ####                ####  ",
		" ##    #####    #####    ## ",
		"##   ##    ##  ##    ##   ##",
		"#    ##    ##  ##    ##    #",
		"       #####    #####      #",
		"#            ##            #",
		"#    ###            ###    #",
		"##     ####      ####      #",
		" ##       ########       ## ",
		"  ####                ####  ",
		"     ####          ####     ",
		"        ############        "};
	steps = SearchExit(labir, Y_MAX/2, X_MAX/2);
	printf("Steps cnt = %d\n", steps);
	return 0;
}