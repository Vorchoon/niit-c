/*6. �������� ���������� ����������� �������, ����������� n-��
������� ���� ���������, �� ��� ��������������� �������� ��������.
���������:
����� ������� ��� �������: ���� ���������� ��������������� �� main
� �������� ������, ���������������, ������� � �������� �����������.*/
#include<stdio.h>

typedef unsigned long long ULL;
ULL fib_iter(ULL prev_val, ULL val, int num_fib)
{
	if(num_fib == 1)
		return val;
	return fib_iter(val, prev_val+val, num_fib-1);
}
ULL fib(int num_fib)
{
	return fib_iter(0, 1, num_fib);
}
int main()
{
	int num_fib;
	printf("Please, enter number (fron 1 to 70) sequence of Fibonacci: ");
	scanf("%d", &num_fib);
	printf("%d number of Fibonacci is %lld\n", num_fib, fib(num_fib));
	return 0;
}