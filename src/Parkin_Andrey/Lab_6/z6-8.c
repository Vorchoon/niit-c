/*8. �������� ���������, ������� ��������� ������������� ���������
��������������� ���������, ��������� � ���� ��������� ��������� ������.
������������� ��������� 4-� �������� ��������. ������� ���������� 
������������ �������� ��������.
���������:
� ������ ��������� ����� ����������� �������: 0-9,+,-,*,/,(,) ���������
����� ���� ��������, �� ���� ������� �������� �� ������ ����� 3, 8, � �����
���� ��������, ��������, ((6+8)*3) ��� (((7-1)/(4+2))-9). ��������������,
��� ������ � ��������� ������ ���������, �� ���� ���������� �������� �����
���������� �������� � ��� �� ���������� ��������.
��������� ������ �������� �� ��������� �������:
(a) int main(int argc, char* argv[]) - ������� �������, � ������� ��������������
	����� ����������� ������� eval ��� ���������� ���������
(b) int eval(char *buf) - �������, ����������� ������, ������������ � buf
(c) char partition(char *buf, char *expr1, char *expr2) - �������, �������
	��������� ������, ������������ � buf �� ��� �����: ������ � ������ ���������,
	���� �������� � ������ �� ������ ���������.*/
#include<stdio.h>
#include<math.h>
#define N 256

int GetInt(char* buf)
{
	int i, result=0, length;
	length = strlen(buf);
	for(i = 0; i < length; i++)
		result += (buf[i] - '0')*pow(10,(length - 1 - i));
	return result;
}

int CheckExpr(char* expr) 
{
	int i, len, in_cnt=0, out_cnt=0;
	len = strlen(expr);
	for(i = 0; i < len; i++)
	{
		if(expr[i] == '(')
			in_cnt++;
		else if(expr[i] == ')')
			out_cnt++;
		if(expr[i] < 40 || expr[i] == 44 || expr[i] == 46 || expr[i] > 58)
		{
			printf("Invalid symbols!!!\n");
			return -1;
		}
	}
	if(in_cnt != out_cnt)
	{
		printf("Different number of input and output brackets!!!\n");
		return -1;
	}
	return 0;
}

int Partition(char* expr, char* left, char* right, char* op)
{
	int len, i, j, cnt = 0, op_position = -1;
	len = strlen(expr);
	// �������, ��� ����� ��������� ������ ���� � �������, ����� ������� ��� ������
	if(expr[0] == '(')
	{
		//����� ������� �����������(*,+,-,/)
		for(i = 1; i < len - 1; i++)
		{
			if(expr[i] == '(')
				cnt++;
			else if(expr[i] == ')')
				cnt--;
			else if(cnt == 0 && (expr[i] == '+' || expr[i] == '-' || expr[i] == '*' || expr[i] == '/'))
			{
				op_position = i;
				break;
			}
		}
		if(op_position < 0)
			return -1; // ���������� ������
		// ��������� ����� �����
		j = 0;
		for(i = 1; i < op_position; i++)
			left[j++] = expr[i];
		left[j] = '\0';
		// ��������� ������ �����
		j = 0;
		for(i = op_position + 1; i < len - 1; i++)
			right[j++] = expr[i];
		right[j] = '\0';
		// ��������
		*op = expr[op_position];
	}
	else
	{
		// �������, ��� ��� ����� � ���������� ��� � ����� �����
		j = 0;
		for(i = 0; i < len; i++)
			left[j++] = expr[i];
		left[j] = '\0';
	}
	return 0;
}

int Eval(char* expr)
{
	char left[N], right[N], op;
	int p_res;
	p_res = Partition(expr, left, right, &op);
	if(op == '+')
		return Eval(left) + Eval(right);
	else if(op == '-')
		return Eval(left) - Eval(right);
	else if(op == '*')
		return Eval(left) * Eval(right);
	else if(op == '/')
		return Eval(left) / Eval(right);
	return GetInt(left);
}

int main(int argc, char* argv[])
{
	int result;
	if(argc != 2)
	{
		printf("Invalid params number!!!\n");
		return 1;
	}
	if(CheckExpr(argv[1]) < 0)
	{
		printf("Check expression error!!!\n");
		return 2;
	}
	result = Eval(argv[1]);
	printf("%s = %d\n", argv[1], result);
	return 0;
}