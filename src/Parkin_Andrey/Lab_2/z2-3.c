/*3. �������� ���������, ��������� �� ����� ����������� �� ��������
���������:
����������� ������ ��������� ���:
  *
 ***
*****
���������� ����� ������� ������������� � ����������.*/
#include<stdio.h>
#define N 120

int main()
{
	int star_i, i, rows, width, space_count, star_count;
	char stars[N];
	printf("Please, enter rows number (from 1 to %d)\n", N/2);
	while(1)
	{
		scanf("%d", &rows);
		if(rows > N/2 || rows < 1)
			printf("Please, try again!\n");
		else
			break;
	}
	width = 2*(rows-1)+1;
	for(i=0; i<rows; i++)
	{
		space_count = width/2 - i;
		star_count = 2*i + 1;
		for(star_i=0; star_i < star_count; star_i++)
			stars[star_i] = '*';
		stars[star_i] = '\0';
		printf("%*s\n", space_count + star_count, stars);
	}
	return 0;
}
