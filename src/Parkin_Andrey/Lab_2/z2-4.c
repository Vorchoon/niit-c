/*4. �������� ���������, �������������� ������� � ������� �������� �������:
������� ���� ��������� �����, ����� �����. ������ �������� � ���� �������-
�� � ���� ��������� ������������������ ���� � ����. ������������ �������-
�������� ��������� ������.
���������:
���������� � ������ ������ ����������� ����� �� ������������. ����� ��-
���������� ����������� ��������� �������.*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>

#define str_len 100

int main()
{	
	char rnd_string[str_len+1], delta_char;
	int rnd_i, rnd_j;
	int x, y;
	srand(time(NULL));
	for(rnd_i = 0; rnd_i < str_len; rnd_i++)
	{
		x = 1+rand()%3;
		switch(x)
		{
			case 1: //Digits (from '48' to '57');
				y = '0' + rand()%('9'-'0'+1);
				break;
			case 2: //Big Liters (from '65' to '90');
				y = 'A' + rand()%('Z'-'A'+1);
				break;
			case 3://Small liters (from '97' to '122');
				y = 'a' + rand()%('z'-'a'+1);
				break;
			default:
				printf("ERRRROOORRRR\n");
		}
		rnd_string[rnd_i] = (char)y;
		//printf("x=%d %c - y=%d\n", x, rnd_string[rnd_i], y);
	}
	rnd_string[rnd_i] = '\0';
	printf("Random string = %s\n", rnd_string);
	for(rnd_i = 0; rnd_i < str_len; rnd_i++)
	{
		if(rnd_string[rnd_i] >= '0' && rnd_string[rnd_i] <= '9')
		{
			delta_char = rnd_string[rnd_i];
			for(rnd_j = str_len-1; rnd_j > rnd_i; rnd_j--)
			{
				if  ((rnd_string[rnd_j] >= 'a' && rnd_string[rnd_j] <= 'z') ||
					(rnd_string[rnd_j] >= 'A' && rnd_string[rnd_j] <= 'Z'))
				{
					rnd_string[rnd_i] = rnd_string[rnd_j];
					rnd_string[rnd_j] = delta_char;
					break;
				}
			}
		}
	}
	printf("Result string = %s\n", rnd_string);
	return 0;
}
