#include <stdio.h>

int main(void)
{
    const float pi = 3.14;
    const float hc = 180;
    float an;
    char tp;
    
     
    scanf("%f %c", &an, &tp);
    
    if (tp == 'D' || tp == 'd')
        printf("%.2f degrees equally %.2f radian", an, (an*pi)/hc);
    
    else if (tp == 'R' || tp == 'r')
        printf("%.2f radian equally %.2f degrees", an, (an*hc)/pi);    
        
        
    return 0;    
    
}